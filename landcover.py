import numpy as np
import matplotlib.pyplot as plt

map_array = np.fromfile('gl-latlong-1km-landcover.bsq', dtype=np.uint8)
map_array = map_array.reshape(21600,43200)

direction_dict = {
    "N" : 1,
    "E" : 1,
    "S" : -1,
    "W" : -1,
}

terrain_dict = {
    0 : "water",
    1 : "evergreen needleleaf forest",
    2 : "evergreen broadleaf forest",
    3 : "deciduous needleleaf forest",
    4 : "deciduous broadleaf forest",
    5 : "mixed forest",
    6 : "woodland",
    7 : "wooded grassland",
    8 : "closed shrubland",
    9 : "open shrubland",
    10 : "grassland",
    11 : "cropland",
    12 : "bare ground",
    13 : "urban and built"
}


def degrees_to_pixels(input_str="48.95˚N 9.13˚E"):

    if "˚" in input_str:
        lat_direction = input_str.split(" ")[0].replace("˚", " ").split(" ")[1]
        lon_direction = input_str.split(" ")[1].replace("˚", " ").split(" ")[1]
        lat_coord = float(input_str.split(" ")[0].replace("˚", " ").split(" ")[0])
        lon_coord = float(input_str.split(" ")[1].replace("˚", " ").split(" ")[0])
    else:
        lat_direction = input_str.split(" ")[1]
        lon_direction = input_str.split(" ")[3]
        lat_coord = float(input_str.split(" ")[0])
        lon_coord = float(input_str.split(" ")[2])

    lat_direction = direction_dict[lat_direction]
    lon_direction = direction_dict[lon_direction]
    
    lat_coord = int((90-lat_coord)/0.008333)
    lon_coord = int((180+lon_coord)/0.008333)
    
    return lat_direction*lat_coord, lon_direction*lon_coord

input_coord = input("Input coordinates in the following format:\n48.95˚N 9.13˚E\n")
lat_pix, lon_pix = degrees_to_pixels(input_coord)

print(map_array[lat_pix, lon_pix])
vis_box_x0 = max((lon_pix - 750),0)
vis_box_y0 = max((lat_pix - 750),0)
plt.imshow(map_array[vis_box_y0:vis_box_y0+750:, vis_box_x0:vis_box_x0+750:])
plt.title(f"Coordinates {input_coord}\npoint to {terrain_dict[map_array[lat_pix, lon_pix]]}")
plt.show()
